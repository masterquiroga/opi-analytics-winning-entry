# OPI Analytics Winning Entry

The winning entry for the Data Engineering test from OPI Analytics.

## How to run this?

This will assume that you've already setup your local Google Cloud CLI properly. 

### 1 Create a new Dataproc cluster in GCP
  It is recommended to have a previous bucket with all the datasets inside `gs://${BUCKET_NAME}/data/in/*`.
  But assumming this is a fresh start just run:
  ```
  gcloud beta dataproc create ${CLUSTER_NAME}\
		--region ${REGION} \
  	--zone ${ZONE} \
  	--master-machine-type n1-standard-8 \
  	--master-boot-disk-size 60 \
  	--worker-machine-type n1-standard-8 \
  	--worker-boot-disk-size 60 \
  	--num-workers 4 \
  	--image-version 1.4-deb9 \
		--optional-components=JUPYTER,ANACONDA \
    --enable-component-gateway
  ```
  Wait until the cluster is created along with its associated bucket.
  
  Note that we use the `beta` version of the command so `--image-version`, `--optional-components`, `--enable-component-gateway` allow us to: use Spark SQL 1.4, satisfy the cool data science deps for pyspark jobs and enable access to the web UIs like Jupyter.

### 3 Upload datasets to cluster bucket
  If you've used a bucket having previously uploaded the datasets, you can safely skip this.

  We'll use the raw datasets placing them in `./data/in` as follows:
  - profeco.pdf.zip
  - demo.csv
  - tweets.json.zip

  With the datasets in place, set `BUCKET_NAME` as the associated bucket used in the cluster and then run:
  ```
  gsutil rsync -J -e -d -r ./data/in gs://${BUCKET_NAME}/data/in
  ```
  This IS going to take a while, like 10 hours or so with mexican internet, so better do this at night. 
  
  Note that `rsync` has the battle-tested benefits of being safer for partial uploads, in case you need to resume from a failed upload and also lets you run a diagnostic dry-run with the `-n` flag, should you need to verify potential problems.
  
### 4 Run the jobs in the cluster
  You're almost there, run the jobs as follows.
  ```
  gcloud dataproc jobs submit pyspark \
    --region ${REGION}
    --cluster ${CLUSTER_NAME} \
    ./jobs/exercice-$n.py -- ${BUCKET_NAME} \
  ``` 
  Where `$n` can be one of `1`, `2` or `3`, and just enjoy!


### Some recommendations

In a production environment it would be better to use a task automator
like `jenkins` or `make` and map equivalent `workflow-templates` for 
the jobs inside the tasks.