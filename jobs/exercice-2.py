# Copyright 2019 Víctor Gerardo González Quiroga
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import sys
import warnings
import pyspark as ps
from pyspark.sql import SparkSession, functions as F


# Assign bucket where the data lives
try:
  storage_bucket = sys.argv[1]
except IndexError:
  print("Please provide a bucket name")
  sys.exit(1)

# Initialize the spark session
spark = SparkSession \
  .builder \
  .master('yarn') \
  .appName('exercice-2') \
  .getOrCreate()
#  .config('spark.executor.cores', '1')\
#  .config('spark.executor.memory', '2688m') \
sc = spark.sparkContext
sc.setLogLevel("ERROR")
sqlc = ps.sql.SQLContext(sc)
print('Created the SparkContext')

# Obtains demo.csv data from GCP bucket
"""
Note that we could map `null` to 'null' in order to allow
the inner join for the last steps using the artificial
'null' country.

One can also use Column.eqNullSafe i.e. coalescences
in order to allow inner join on null fields, but since 
nobody uses 'null' as a country name, 
it's not as readable nor pragmatic.
"""
data_file = 'demo.csv'
try:
  spark.sql(f"""
    CREATE OR REPLACE TEMPORARY VIEW demo
    USING csv
    OPTIONS (
      path "gs://{storage_bucket}/data/in/{data_file}",
      header TRUE,
      inferSchema TRUE,
      nullValue ''
    )
  """)
except Exception as e:
  spark.stop()
  sys.exit(str(e))

df_demo = spark.sql("SELECT * FROM demo")


print('Successfully read demo.csv!')

# Let's see how demo.csv looks inside
df_demo.printSchema()

# ------
# Step 1
print("\n1. Generating dictionary from column country (country_id, country)")


keypair_countries = spark.sql("""
  SELECT monotonically_increasing_id() AS country_id, country
  FROM (
    SELECT DISTINCT country FROM demo
  ) 
""").rdd.map( \
  lambda x: (x[0], x[1])) #  ("country_id", country)
#df_countries.show()

# Let's see the resulting dict
dict_countries = keypair_countries.collectAsMap()
print(dict_countries)


# ---------------------
# Step 2, read carefuly
"""
Note that dictionaries cannot be saved from spark 
per se, we would have to do something in idiomatic 
python like this:
`
with open('/tmp/countries.csv','w') as f:
  w = csv.writer(f)
  w.writerows(dict_countries.items())
`
Actually that is sooo much faster, due to not having
to bridge to the Scala interpreter in the JVM, with
the tradeoff of using local storage.

*But*, to write the RDD 'dictionary' using Spark is 
just as easy as follows:
"""
print("\n2. Writing dictionary CSV from spark...")
# Recall keypair_countries looks like  (country_id, country)
sqlc.createDataFrame(keypair_countries, ['country_id', 'country']) \
  .coalesce(1) \
  .write \
  .format('com.databricks.spark.csv') \
  .mode('overwrite') \
  .options(header=True) \
  .save(f'gs://{storage_bucket}/data/out/countries.csv')
    
print("countries.csv saved successfully!")


# Last step
print("\n3. Reading generated CSV and inner joining...")
try:
  spark.sql(f"""
    CREATE OR REPLACE TEMPORARY VIEW countries
    USING csv
    OPTIONS (
      path "gs://{storage_bucket}/data/out/countries.csv",
      header TRUE,
      inferSchema TRUE
    )
  """)
except Exception as e:
  spark.stop()
  sys.exit(str(e))
#df_countries_.show()
df_result = spark.sql("""
  SELECT * FROM demo
  INNER JOIN countries
  ON (
    TRIM(demo.country) IS NOT DISTINCT FROM TRIM(countries.country)
    OR (coalesce(demo.country, '') = coalesce(countries.country, ''))
    OR coalesce(demo.country, countries.country) IS NULL
  )
""")
print("Successfully inner joined generated csv with demo.csv!")
#df_result.show()


# --------------
# FINAL SHOWDOWN
spark.sql("""
  SELECT * FROM demo
  ANTI JOIN countries
  ON (
    TRIM(demo.country) IS NOT DISTINCT FROM TRIM(countries.country)
    OR (coalesce(demo.country, '') = coalesce(countries.country, ''))
    OR coalesce(demo.country, countries.country) IS NULL
  )
""").show()
n1 = df_demo.count()
n2 = df_result.count()

same_number_of_records = n1 == n2
print("Expected count:", n1)
print("Resulting count:", n2)
if (same_number_of_records):
  print("Same number of records, we won! :D")
else:
  print("Not the same number of records...")

spark.stop()